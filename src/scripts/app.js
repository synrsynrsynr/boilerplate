import '@babel/polyfill';
import 'whatwg-fetch';
import 'lazysizes';
import '../styles/app.css';

const Top = import(/* webpackChunkName: "top" */ './templates/top');
const CommonFunction = import(/* webpackChunkName: "top" */ './components/commonFunction');

const app = async () => {
  CommonFunction.then(async func => {
    const { Fetch } = func.default
    const result = await Fetch('/api/sample/', {mode: 'cors', credentials: 'include'})
    console.log(result)
  })
  Top.then( func => func.default() );
}

window.addEventListener('DOMContentLoaded', app());
