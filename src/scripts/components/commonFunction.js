const CommonFunction = {
  async Fetch (
    url = '/api/sample/',
    params = {
      mode: 'cors',
      credentials: 'include',
    },
  ) {
    try {
      return await fetch(url, params)
        .then(res => res.json());
    } catch (e) {
      console.log(e);
      return {
        result: {
          msg: 'fail',
        },
      };
    }
  }
}

export default CommonFunction;
