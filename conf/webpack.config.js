const fs = require('fs');
const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const EncodingPlugin = require('webpack-encoding-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const Copy = require('copy-webpack-plugin');
const Clean = require('clean-webpack-plugin');

const Resolve = path.resolve;
const context = ReplacePath(Resolve('./'));

const revisionId = fs.readFileSync(ReplacePath(Resolve('./conf/.revisionId')), 'utf8');
const scriptPath = ReplacePath(Resolve('./src/scripts/'))
const targets = glob.sync(`${scriptPath}/*.js`);
const entries = {};

targets.forEach((value) => {
  const re = new RegExp(`${scriptPath}/`);
  const key = value.replace(re, '').replace('.js', '');
  entries[key] = value;
});

function ReplacePath(path) {
  return path.replace(/\\/g, '/');
}

module.exports = (params) => {
  const { env } = params;
  const { mode } = env;
  return {
    mode,
    context: Resolve('./src'),
    entry: entries,
    output: {
      path: Resolve('./public/static/js/'),
      filename: `[name]-${revisionId}.js`,
      publicPath: `${env.js}`,
    },
    resolve: {
      extensions: ['.css', '.js', '.html'],
      modules: [
        './node_modules',
      ],
      alias: {},
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          enforce: 'pre',
          loaders: 'eslint-loader',
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: ["@babel/plugin-syntax-dynamic-import"]
            }
          }
        },
        {
          test: /\.css$/,
          // test: /\/styles\/.*\.css$/,
          exclude: /node_modules/,
          use: [
            ExtractCssChunks.loader,
            "css-loader",
            "postcss-loader",
          ],
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                emitFile: false,
                name: '../[path][name].[ext]',
                publicPath: Path => {
                  const path = env.img + Path.replace('../images/', '');
                  return path;
                },
              },
            },
          ],
        },
      ],
    },
    optimization: {
      minimizer: [
        new UglifyJSPlugin(
          {
            uglifyOptions: {
              output: { comments: /^\**!|@preserve|@license|@cc_on/ },
            },
          },
        ),
      ],
      splitChunks: {
        name: 'vendor',
        chunks: 'initial',
      },
    },
    plugins: [
      new HardSourceWebpackPlugin(),
      new Clean(
        ['public'],
        { root: context },
      ),
      new ExtractCssChunks({
        filename: `../css/style-${revisionId}.css`,
        chunkFilename: "[id].css",
      }),
      new webpack.DefinePlugin({
        ENV: JSON.stringify(env),
      }),
      new Copy(
        [
          {
            from: path.join(context, 'src/images'),
            to: path.join(context, 'public/images'),
          },
          {
            context: 'html',
            from: '**/*.html',
            to: path.join(context, 'public'),
            ignore: [
              'node_modules/**/*.html',
              'public/**/*.html',
            ],
            transform: (content) => {
              let change = content;
              const target = {
                css: String(content).match(/\/css\/[a-zA-Z0-9`]*.[a-zA-Z]*/g),
                js: String(content).match(/\/js\/[a-zA-Z0-9]*.[a-zA-Z]*/g),
              };
              Object.keys(target).forEach((key) => {
                if (target[key] != null) {
                  target[key].forEach((reg) => {
                    const name = reg.replace(`.${key}`, '').replace(`/${key}/`, '');
                    const replace = `${env[key]}${name}-${revisionId}.${key}`;
                    change = String(change).replace(reg, replace);
                  });
                }
              });
              change = String(change).replace(/\/images\//g, `${env.img}`);
              return change;
            },
          },
        ],
        { debug: false },
      ),
      new EncodingPlugin(
        { encoding: 'utf-8' },
      ),
    ],
  };
};
