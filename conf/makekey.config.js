const uuidv1 = require('uuid/v1');
const fs = require('fs');

let revisionId = uuidv1();
revisionId = revisionId.replace(/\-/g, '');
fs.writeFile('conf/.revisionId', revisionId);
