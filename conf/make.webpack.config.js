const env = require('./env');

module.exports = require('./webpack.config')({
  env,
});
