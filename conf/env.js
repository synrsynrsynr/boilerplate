
if (!process.env.TARGET || process.env.TARGET === 'local') {
  module.exports = {
    img: '/images/',
    js: '/static/js/',
    css: '/static/css/',
    mode: 'development',
  };
}

if (process.env.TARGET === 'dev') {
  module.exports = {
    img: '/images/',
    js: '/static/js/',
    css: '/static/css/',
    mode: 'development',
  };
}

if (process.env.TARGET === 'staging') {
  module.exports = {
    img: '/images/',
    js: '/static/js/',
    css: '/static/css/',
    mode: 'production',
  };
}

if (process.env.TARGET === 'production') {
  module.exports = {
    img: '/images/',
    js: '/static/js/',
    css: '/static/css/',
    mode: 'production',
  };
}
