const express = require('express');
const http = require('http');
const httpProxy = require('http-proxy');
const logger = require('morgan');
const ssi = require('connect-ssi');
const apiConfig = require('./api.config');

const api = httpProxy.createProxyServer();
const app = express();
const server = http.createServer(app);

const path = require('path');
const Resolve = path.resolve;
const context = ReplacePath(Resolve('./'));

function ReplacePath(path) {
  return path.replace(/\\/g, '/');
}

app.set('port', process.env.PORT || 8080);
app.use(logger('dev'));
app.use(ssi({
  baseDir: `${context}/public`,
  encoding: 'utf-8',
  ext: '.html',
}));
app.use(express.static(`${context}/public`));

server.listen(app.get('port'), () => {
  console.log(`listening on port ${app.get('port')}`);
});

apiConfig.isApi ?
  app.all("/api/*", function(req, res) {
    api.web(req, res, {target: apiConfig.proxy});
  }) : '';
